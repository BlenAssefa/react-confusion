import React from 'react';
import {
    Card, CardImg, CardText, CardBody, CardTitle,
    ListGroupItemText,
    Breadcrumb,
    BreadcrumbItem

} from 'reactstrap';

import { Link } from 'react-router-dom';


/*
componentDidMount() {
    console.log('DishDetail Components componentDidMount is Invocked');
}

componentDidUpdate() {
    console.log('DishDetail Components componentDidUpdate is Invocked');
} */


function RenderDish({ dish }) {
    if (dish != null) {
        return (
            <Card>
                <CardImg width="100%" src={dish.image} alt={dish.name} />
                <CardBody>
                    <CardTitle>
                        {dish.name}
                    </CardTitle>
                    <CardText>
                        {dish.description}
                    </CardText>
                </CardBody>
            </Card>
        );
    } else {
        return (
            <div></div>
        );
    }
}

function RenderDishComments({ comments }) {
    if (comments != null) {

        return (
            <div className="container">
                <h2>Comments</h2>
                <div>

                    {comments.map((comment) => (
                        <div key={comment.id}>
                            <ListGroupItemText>
                                {comment.comment}<br></br>
                                        --{comment.author}, {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit' }).format(new Date(Date.parse(comment.date)))}

                            </ListGroupItemText>

                        </div>

                    ))}
                </div>
            </div>
        );

    } else {
        return (
            <div></div>
        );
    }
}

const DishDetail = (props) => {
    console.log('DishDetail Components render is Invocked');
    return (
        <div className="container">
            <div className="row">
                <Breadcrumb>
                    <BreadcrumbItem>
                        <Link to="/home">Home
            </Link>
                    </BreadcrumbItem>
                    <BreadcrumbItem>
                        <Link to="/menu">Menu
            </Link>
                    </BreadcrumbItem>
                    <BreadcrumbItem active>{props.dish.name}
                    </BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3> {props.dish.name}</h3>
                    <hr />
                </div>
            </div>
            <div className="row">
                <div className="col-12 col-md-5 m-1">
                    <RenderDish dish={props.dish} />
                </div>
                <div className="col-12 col-md-5 m-1">
                    <RenderDishComments dish={props.comments} />
                </div>
            </div>
        </div>
    );
}


export default DishDetail;